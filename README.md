# MintmeOAuth

## Description
A small script for geting the beare token from mintme.com page in order to use the API.

I had to use the API of the minteme.com page and used the documentation page (https://www.mintme.com/dev/documentation/v2), but in order to use anything under /dev/api/v2/auth/ you have to authenticate your requests. Since I did not find a quick way to obtain the bearer token, I wrote this little script.

## Setup
```
git clone git@gitlab.com:musdasch/mintmeoauth.git
cd minteoauth
npm install
```

## Usage
In order to obtain the bearer token, you have to set up your client for oauth access to API. You will find the option under https://www.mintme.com/settings after login. After setting up your client, you will get an ID and a secret, both have to be entered in the terminal after starting the script.

```
cd [folder of the script]
npm start

Client ID: [Enter your ID]
Client Secret: [Enter your secret]

Bearer Token: [Your bearer token will show]
```

## License
Copyright © 2022 musdasch <musdasch@protonmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
