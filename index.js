import axios from 'axios';
import terminal from 'terminal-kit';

const term = terminal.terminal;

const authBaseUrl = 'https://www.mintme.com/oauth/v2/';



function input() {
  return new Promise((resolve, reject) => {
    term.inputField((error,input) => {
      resolve(input);
    });
  });
}


(async () => {
  term.green('Client ID: ');
  const clientID = await input();
  term('\n');

  term.green('Client Secret: ');
  const clientSecret = await input();
  term('\n\n');

  const bearerToken = await axios.get(
    authBaseUrl + 'token',
    {
    params: {
      'grant_type'  : "client_credentials",
      'client_id'   : clientID,
      'client_secret' : clientSecret
    }
    }
  );

  if(bearerToken.status == 200) {
    term.green('Bearer Token: ');
    term(bearerToken.data.access_token);
    term('\n');
  } else {
    term.red('Response error:\n');
    console.log(bearerToken);
  }

  process.exit(1);
})();